var userModel=require('../Model/userModel');

var findUser=function(userData){

  return new Promise((resolve)=>{
    userModel.findOne({userName:userData},(err,dbres)=>{
      if(err){
        throw err;
      }
      if(dbres===null){
          resolve(false);
      }
      else{
        resolve(true);
      }
    })
  })
  
  }
  var userDao={};
  userDao.findUser=findUser;
  module.exports=userDao;
