const express=require('express');
const app = express();
const ejs = require('ejs');
const bodyParser = require('body-parser');
const urlencoder = bodyParser.urlencoded({extended:false});
var userService=require('../userService/userService');
var router=express.Router();

router.route('/')
.get((req,res)=>{
  res.render('index');
});

router.route('/register')
.get((req,res)=>{
  res.render('register');
});

router.route('/login')
.get((req,res)=>{
  res.render('login');
});

router.route('/RegSuccess')
.post(urlencoder,(req,res)=>{
  var userData={};
  userData.userName=req.body.UserName;
  userData.Password=req.body.pwd;
  userData.Telephone=req.body.tele;
  userData.Gst=req.body.gst;
  userData.Country=req.body.country;
  userData.firstName=req.body.fname;
  userData.lastName=req.body.lname;
  userService.registration(userData).then((result)=>{
    if(result.exist){
      res.redirect('/register');
    }
    else{
      res.redirect('/login')
    }
  })
})

module.exports=router;
