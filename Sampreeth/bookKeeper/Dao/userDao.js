const userData = require('../Model/userDataModel');
var find = function (username) {
	return new Promise(resolve => {
		dbUserData = {};
		userData.findOne({ userName: username }, function (err, dbresp) {
			if (err) {
				throw err;
			}
			if (dbresp === null) {
			dbUserData.found = false;

				resolve(dbUserData);
			} else {
				dbUserData.found = true;
				dbUserData.userObj = dbresp;
			}
			resolve(dbUserData);
		});
	});
}
var save = function (userDbObject) {
	
	var userInfo = new userData({
        userName:userDbObject.userName,
       tele:userDbObject.tele,
       userPassword: userDbObject.userPassword,
       Fname: userDbObject.fName,
       Lname: userDbObject.lName,
       Gst: userDbObject.Gst,
       country: userDbObject.userCountry
     });
	userInfo.save(function (err, DBresponse) {
		if (err) {
			throw err;
		}
	});

}
var passwordValid = function (userDbData) {
	return new Promise(resolve => {
		userData.findOne({ userName: userDbData.userName }, function (err, dbData) {
			if (err)
				throw err;
			if (dbData.userPassword === userDbData.userPassword) {
				resolve(true);
			}
			else {
				resolve(false);
			}
		})
	});
}
var userDao = {};
userDao.find = find;
userDao.save = save;
userDao.passwordValid = passwordValid;
module.exports = userDao;