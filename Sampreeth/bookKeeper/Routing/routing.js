const express = require('express');
const bodyParser = require('body-parser');

const userService = require('../Service/userService');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
const app = express();

var router = express.Router();
router.route('/')
	.get(function (req, res) {
		res.render('index');
    });
    router.route('/login.ejs')
     .get(function(req,res){
         msg1='';
         res.render('login',{msg:msg1});
     })
    router.route('/register.ejs')
      .get(function(req,res){
          msg1=''
          res.render('register',{msg:msg1});
      })
      router.route('/RegSuccess')
	.post(urlencodedParser, function (req, res) {
		var userData = {};
		userData.userName = req.body.UserName;
		userData.userPassword =req.body.pwd;
        userData.userCountry=req.body.country
        userData.tele=req.body.tele;
        userData.fName=req.body.fname;
        userData.lName=req.body.lname;
		userData.Gst=req.body.gst;
		console.log(userData);
		userService.registerIfNewUser(userData).then((result) => {
			console.log('router register, userService result', result);
			if (result.saved) {
				res.redirect('/login.ejs');
				// redirect to login page
			}
			else {
				res.render('register', { msg: 'User Name Exists' });
				// user already exist
			}
		},
			(error) => {
				console.log(error);
			});
    });
    router.route('/sucess')
	.post(urlencodedParser, function (req, res) {
		var loginUserData = {};
		loginUserData.userName = req.body.UserName;
		loginUserData.userPassword = req.body.pwd;
		userService.loginOnValidation(loginUserData).then((result) => {
			console.log('result in login:',result);
			console.log('username in login:',result.Obj.userName);
		res.render('invoice',{userName:result.Obj.userName,userPassword:result.Obj.userPassword,country:result.Obj.country,tele:result.Obj.tele,Fname:result.Obj.Fname,Lname:result.Obj.Lname,Gst:result.Obj.Gst,});
		}, (error) => {
			console.log(error);
			res.render('login', { msg: 'You have entered username or Password Wrong!Please Try Again' });
		})
	})
    module.exports=router;
